import com.toedter.calendar.JCalendar;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static javax.swing.JFileChooser.DIRECTORIES_ONLY;

public class MainWindow extends JFrame {

    private JPanel panel;
    private JButton copyBtn;
    private JButton sourceBtn;
    private JButton destinationBtn;
    private JTextField sourceText;
    private JTextField destinationText;
    private JButton fromDatePickerBtn;
    private JButton toDatePickerBtn;
    private JCalendar calendarDatePicker;
    private JTextField fromDatePickerText;
    private JTextField toDatePickerText;
    private JCheckBox saveLogsToFileCheckBox;
    private File pathToSource;
    private File pathToDestination;
    private Date fromDate = new Date();
    private Date toDate = new Date();

    private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    public class CopyButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent actionEvent){

            int numberOfCopies = 0;

            if (fromDate.getTime() >= toDate.getTime() || fromDate == null || toDate == null){
                JOptionPane.showMessageDialog(null, "Error.\n Selected invalid from/to date.");
                fromDatePickerText.setText("");
                toDatePickerText.setText("");
                return;
            }

            if (pathToSource == pathToDestination || pathToSource == null || pathToDestination == null || !pathToSource.exists() || !pathToDestination.exists()){
                JOptionPane.showMessageDialog(null, "Error.\n Selected invalid path to files/destination.");
                sourceText.setText("");
                destinationText.setText("");
                return;
            }

            Copier copier = new Copier();
            String pathToSourceString = pathToSource.toString();
            String pathToDestinationString = pathToDestination.toString();
            File directory = new File(pathToSourceString);
            File[] directoryListing = directory.listFiles();

            if (directoryListing != null) {
                for (File child : directoryListing) {
                    if (child.lastModified() > fromDate.getTime() && child.lastModified() < toDate.getTime() ){
                        if ( new File( pathToDestinationString + File.separator + child.getName()).exists()){
                            continue;
                        }
                        try {
                            copier.copyFileUsingChannel(child, new File(pathToDestinationString + File.separator + child.getName()));
                            numberOfCopies++;
                        }catch (IOException e){
                            JOptionPane.showMessageDialog(null, "Unknown error.");
                            e.printStackTrace();
                        }
                    }
                }

                if (numberOfCopies > 0) {
                    JOptionPane.showMessageDialog(null, "Successfully copied : " + numberOfCopies + " files.");

                    if (saveLogsToFileCheckBox.isSelected()){
                        Date actuallyDate = new Date();
                        String lineSeparator = System.getProperty("line.separator");
                        FileWriter fileWriter = new FileWriter();
                        fileWriter.writeUsingOutputStream("At : " + actuallyDate.toString() + " was successfully copied : " + numberOfCopies + " files." + lineSeparator, pathToDestinationString);
                    }
                }
                else {
                    JOptionPane.showMessageDialog(null, "No files copied.");
                }

            }
        }
    }

    private class PathToSourceButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent actionEvent){

            JFileChooser openDirectory = new JFileChooser();
            openDirectory.setFileSelectionMode(DIRECTORIES_ONLY);
            int ret = openDirectory.showDialog(null, "Open directory");
            if (ret == JFileChooser.APPROVE_OPTION) {
                pathToSource = openDirectory.getSelectedFile();
                sourceText.setText(pathToSource.toString());
            }
        }
    }

    private class PathToDestinationButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent actionEvent){

            JFileChooser openDirectory = new JFileChooser();
            openDirectory.setFileSelectionMode(DIRECTORIES_ONLY);
            int ret = openDirectory.showDialog(null, "Open directory");
            if (ret == JFileChooser.APPROVE_OPTION) {
                pathToDestination = openDirectory.getSelectedFile();
                destinationText.setText(pathToDestination.toString());
            }
        }
    }

    private class FromDatePickerButtonListener implements ActionListener{

        public void actionPerformed(ActionEvent actionEvent) {

            fromDate = calendarDatePicker.getDate();

            try {
            fromDate = formatter.parse(formatter.format(fromDate));
            } catch (ParseException e){
                JOptionPane.showMessageDialog(null, "Unknown error.");
                e.printStackTrace();
            }
            fromDatePickerText.setText(fromDate.toString());


        }
    }

    private  class  ToDatePickerButtonListener implements  ActionListener{

        public void actionPerformed(ActionEvent actionEvent){

            toDate = calendarDatePicker.getDate();

            try {
                toDate = formatter.parse(formatter.format(toDate));
            } catch (ParseException e){
                JOptionPane.showMessageDialog(null, "Unknown error.");
                e.printStackTrace();
            }

            Calendar nexDay = Calendar.getInstance();
            nexDay.setTime(toDate);
            nexDay.add(Calendar.HOUR, 23);
            nexDay.add(Calendar.MINUTE,59);
            nexDay.add(Calendar.SECOND,59);
            toDate = nexDay.getTime();

            toDatePickerText.setText(toDate.toString());

        }
    }


    public MainWindow() {

        this.getContentPane().add(panel);
        this.copyBtn.addActionListener(new CopyButtonListener());
        this.sourceBtn.addActionListener(new PathToSourceButtonListener());
        this.destinationBtn.addActionListener(new PathToDestinationButtonListener());
        this.fromDatePickerBtn.addActionListener(new FromDatePickerButtonListener());
        this.toDatePickerBtn.addActionListener(new ToDatePickerButtonListener());

    }


}
