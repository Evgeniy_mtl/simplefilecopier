
//import org.apache.camel.main.Main;

import javax.swing.*;
import java.awt.*;

public class Main{

    public static void main(String... args) {
        MainWindow mainWindow = new MainWindow();
        mainWindow.pack();
        mainWindow.setSize(new Dimension(700,250));
        mainWindow.setVisible(true);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.setTitle("File Copier");
        mainWindow.setLocation(600,350);
    }

}