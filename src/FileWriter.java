import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileWriter {

    public static void writeUsingOutputStream(String data, String pathToLogFile) throws NullPointerException {
        OutputStream os = null;
            try {
                pathToLogFile = pathToLogFile + File.separator + "logFile.txt";
                os = new FileOutputStream( new File(pathToLogFile), new File(pathToLogFile ).exists());
                os.write(data.getBytes(), 0, data.length());
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
